﻿using UnityEngine;
using System.Collections;

public enum GameEventType {
    LevelStarted, //он же уже таки стартанул
    LevelEnd,
    LevelRestart,

    AddFood,
    AddMoney,

    SproutGrow,
    CreateCloud,
    SunflowerGrow,
    SunflowerRemove,

    TurnEnd,
    CommitActs,

    WaterDrop, //add water
    SunShine,  //add sun
    RainStart,

    DisableBlocks,
    EnableBlocks,
}

public enum Param {
    ResourcesCount, //int
}