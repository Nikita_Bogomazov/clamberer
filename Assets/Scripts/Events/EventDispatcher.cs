﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = System.Object;
using Random = System.Random;

public class EventDispatcher {

    private static EventDispatcher instanse;

    private readonly Dictionary<GameEventType, LinkedList<Action<GameEventType, Dictionary<Param, object>>>> listeners =
            new Dictionary<GameEventType, LinkedList<Action<GameEventType, Dictionary<Param, object>>>>();

    private readonly Dictionary<GameEventType, LinkedList<Action<GameEventType, Dictionary<Param, object>>>> toRemove = 
            new Dictionary<GameEventType, LinkedList<Action<GameEventType, Dictionary<Param, object>>>>();

    public static Object locker = new object();

    public static EventDispatcher Instance {
        get {
            if (instanse != null)
                return instanse;
            lock (locker) {
                if (instanse == null) {
                    instanse = new EventDispatcher();
                }
            }
            return instanse;
        }
    }

    private EventDispatcher() {
    }

    public void Clear() {
        foreach (var key in toRemove.Keys) {
            var value = toRemove[key];
            var listListeners = listeners[key];
            foreach (var action in value) {
                listListeners.Remove(action);
            }
        }
        toRemove.Clear();
    }

    public void UnSubscribe(GameEventType ev, Action<GameEventType, Dictionary<Param, Object>> listener) {
        if (listeners.ContainsKey(ev)) {
            listeners[ev].Remove(listener);
        }
    }

    public void Subscribe(GameEventType ev, bool single, Action<GameEventType, Dictionary<Param, Object>> listener) {
        if (!listeners.ContainsKey(ev)) {
            listeners.Add(ev, new LinkedList<Action<GameEventType, Dictionary<Param, object>>>());
        }
        if (!listeners[ev].Contains(listener)) {
            listeners[ev].AddLast(listener);
        }
        
        //if (removeOnReload) {
        //    if (!toRemove.ContainsKey(ev))
        //        toRemove.Add(ev, new LinkedList<Action<GameEventType, Dictionary<Param, object>>>());
        //    if (!toRemove[ev].Contains(listener))
        //        toRemove[ev].AddLast(listener);
        //}
    }

    public void DispatchEvent(GameEventType ev, Dictionary<Param, Object> param = null) {
        if (!listeners.ContainsKey(ev))
            return;
        if (listeners[ev] == null)
            return;
        var list = new LinkedList<Action<GameEventType, Dictionary<Param, object>>>(listeners[ev]);
        
        foreach (var listener in list) {
            listener.Invoke(ev, param);
        }
    }
}
