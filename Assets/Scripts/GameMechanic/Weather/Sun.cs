﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sun : IWeather {
    public GameObject ПрефабЛуча;
    public bool Enabled;

    private Building _building;
    private List<GameObject> _sunLight = new List<GameObject>(); 
    private int _positionX;

    void Start() {
        _building = FindObjectOfType<Building>();
    }

    private void Create() {
        _positionX = 0;
        for (int i = 0; i < _building.Height; i++) {
            var sun = Instantiate(ПрефабЛуча);
            sun.transform.position = _building.BuildingBlocks[i, _positionX].transform.position;
            _sunLight.Add(sun);
        }
        var dict = new Dictionary<Param, object>();
        dict.Add(Param.ResourcesCount, _building.FindSproutsOnTheLine(_positionX,0));
        EventDispatcher.Instance.DispatchEvent(GameEventType.SunShine, dict);
        _positionX++;
    }

    private void Move() {
        if (_positionX >= _building.Width) {
            Disable();
            return;
        }
        for (int i = 0; i < _building.Height; i++) {
            _sunLight[i].transform.position = _building.BuildingBlocks[i, _positionX].transform.position;
        }
        var dict = new Dictionary<Param, object>();
        dict.Add(Param.ResourcesCount, _building.FindSproutsOnTheLine(_positionX,0));
        EventDispatcher.Instance.DispatchEvent(GameEventType.SunShine, dict);
        _positionX++;
    }

    public void Enable() {
        if (Enabled) {
            Move();
            return;
        }
        Create();
        Enabled = true;
    }

    private void Disable() {
        Enabled = false;
        Erase();
    }

    private void Erase() {
        foreach (var sun in _sunLight) {
            Destroy(sun);
        }
    }
}
