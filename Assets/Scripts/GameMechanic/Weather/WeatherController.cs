﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeatherController : MonoBehaviour {
    private Rain _rain;
    private Sun _sun;

	void Start () {
	    _rain = FindObjectOfType<Rain>();
        _sun = FindObjectOfType<Sun>();
	    EventDispatcher.Instance.Subscribe(GameEventType.TurnEnd,false,ImitateWeather);
	}

    public void ForceCreateCloud(bool active, XY position) {
        _rain.ForceCreate(active,position);
    }

    public void ImitateWeather(GameEventType type, Dictionary<Param, object> obj) {
        if (Random.Range(0, 4) == 0) {
            _rain.Create();
        }
        _rain.Move();

        if (!_sun.Enabled) {
            if (Random.Range(0, 2) == 0) {
                _sun.Enable();
            }
        } else {
            _sun.Enable();
        }
    }
}
