﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;

public class RainCloudSettings {
    public RainCloudSettings(GameObject cloud, bool active, int x, int y) {
        Cloud = cloud;
        Active = active;
        PositionX = x;
        PositionY = y;
    }
    public GameObject Cloud;
    public bool Active;
    public int PositionX;
    public int PositionY;
    public bool EnableRain;
    public IEnumerator Coroutine;
}

public class Rain : IWeather {
    public GameObject ПрефабТучи;
    public GameObject ПрефабКапли;

    private List<RainCloudSettings> _clouds = new List<RainCloudSettings>(); 
    private Sky _sky;
    private Building _building;

    void Start() {
        _sky = FindObjectOfType<Sky>();
        _building = FindObjectOfType<Building>();
    }

    public void ForceCreate(bool active, XY position) {
        var positionX = position.Y;
        var positionY = position.X;
        var cloud = Instantiate(ПрефабТучи);
        cloud.transform.position = _sky.SkyParts[positionY, positionX].transform.position;
        _sky.SkyParts[position.X, positionY].GetComponent<SkyParts>().ChangeAction(OnClickSkyAction.Nothing);
        positionX++;
        _clouds.Add(new RainCloudSettings(cloud, active, positionX, positionY));
        EventDispatcher.Instance.DispatchEvent(GameEventType.CommitActs);
    }

   public void Create() {
        var positionX = 0;
        var positionY = Random.Range(4, _sky.Height - 1);
        if(_sky.SkyParts[positionY, positionX].GetComponent<SkyParts>().Engaged)return;
        var cloud = Instantiate(ПрефабТучи);
        cloud.transform.position = _sky.SkyParts[positionY, positionX].transform.position;
        _sky.SkyParts[positionY, positionX].GetComponent<SkyParts>().ChangeAction(OnClickSkyAction.Nothing);
        positionX++;
        _clouds.Add(new RainCloudSettings(cloud, true,positionX,positionY));
    }

    public void Move() {
        if(_clouds.Count < 1)return;
        foreach (var cloudSettigns in _clouds) {
            _sky.SkyParts[cloudSettigns.PositionY, cloudSettigns.PositionX-1].GetComponent<SkyParts>().ChangeAction(OnClickSkyAction.CreateCloud);
            _sky.SkyParts[cloudSettigns.PositionY, cloudSettigns.PositionX].GetComponent<SkyParts>().ChangeAction(OnClickSkyAction.Nothing);
            cloudSettigns.Cloud.transform.position = _sky.SkyParts[cloudSettigns.PositionY, cloudSettigns.PositionX].transform.position;
            if (Random.Range(0, 2) == 0) {
                if (!cloudSettigns.Active)break;
                //rain start
                if (cloudSettigns.Coroutine != null)
                    StopCoroutine(cloudSettigns.Coroutine);
                cloudSettigns.Coroutine = StartRain(cloudSettigns.Cloud);
                StartCoroutine(cloudSettigns.Coroutine);
                cloudSettigns.EnableRain = true;
            } else {
                if (!cloudSettigns.Active)
                    break;
                //rain end
                if (cloudSettigns.Coroutine != null)
                    StopCoroutine(cloudSettigns.Coroutine);
                cloudSettigns.EnableRain = false;
            }
            if (cloudSettigns.PositionX >= Sky.Отступ &&  cloudSettigns.PositionX <= Sky.Отступ + _building.Width &&  cloudSettigns.EnableRain) {
                var value =  cloudSettigns.PositionX - Sky.Отступ;
                var dict = new Dictionary<Param, object>();
                dict.Add(Param.ResourcesCount, _building.FindSproutsOnTheLine(value, cloudSettigns.PositionY - 1));
                EventDispatcher.Instance.DispatchEvent(GameEventType.WaterDrop, dict);
            }
            cloudSettigns.PositionX++;
            if (cloudSettigns.PositionY >= Sky.Отступ*2 + _building.Width) {
                Disable(cloudSettigns);
            }   
        }
    }

    private void Disable(RainCloudSettings settings) {
        StopCoroutine(settings.Coroutine);
        settings.EnableRain = false;
        Destroy(settings.Cloud);
        _clouds.Remove(settings);
    }


    private IEnumerator StartRain(GameObject cloud) {
        while (true) {
            var inst = Instantiate(ПрефабКапли);
            inst.GetComponent<Rigidbody2D>().isKinematic = true;
            inst.transform.position = cloud.transform.position;
            inst.GetComponent<Rigidbody2D>().isKinematic = false;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
