﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleCloud : IAction {
    public override void Action() {
        if (!Limitation(Building.EntangleParams.Block))
            return;
           WeatherController.ForceCreateCloud(true, Sky.CloudParam);
    }

    protected override void AddEvent() {
        EventDispatcher.Instance.DispatchEvent(GameEventType.CreateCloud);
    }

    protected override void PayPrice(GameEventType type, Dictionary<Param, object> obj) {
        CurrentLevelSettings.CurrentResources.SunLight -= SunPrice;
        CurrentLevelSettings.CurrentResources.Water -= WaterPrice;
    }

    protected override void Constructor() {
        EventDispatcher.Instance.Subscribe(GameEventType.CreateCloud, true, PayPrice);
        WaterPrice = 2;
        SunPrice = 1;
        Price.Water = WaterPrice;
        Price.SunLight = SunPrice;
    }

    protected override void Destroyer() {
        EventDispatcher.Instance.UnSubscribe(GameEventType.CreateCloud, PayPrice);
    }

    protected override bool Limitation(XY to) {
        var lim = true;

        if (CurrentLevelSettings.CurrentResources.Water < Price.Water 
        || CurrentLevelSettings.CurrentResources.SunLight < Price.SunLight) {
            Debug.Log("Not enough resources");
            lim = false;
        }
        if (lim) {
            AddEvent();
            return true;
        }
        return false;
    }
}
