﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SunflowerGrow : SproutGrow {
    private const int ADD_SUNLIGHT = 1;

    protected override void AddEvent() {
        EventDispatcher.Instance.Subscribe(GameEventType.TurnEnd, false, Effect);
        EventDispatcher.Instance.DispatchEvent(GameEventType.SunflowerGrow);
    }

    protected override void Effect(GameEventType type, Dictionary<Param, object> obj) {
        CurrentLevelSettings.CurrentResources.SunLight += ADD_SUNLIGHT;
    }

    protected override void Remove(GameEventType type, Dictionary<Param, object> obj) {
        EventDispatcher.Instance.UnSubscribe(GameEventType.TurnEnd, Effect);
    }

    protected override void PayPrice(GameEventType type, Dictionary<Param, object> obj) {
        CurrentLevelSettings.CurrentResources.SunLight -= SunPrice;
        CurrentLevelSettings.CurrentResources.Water -= WaterPrice;
    }

    protected override void Destroyer() {
        EventDispatcher.Instance.UnSubscribe(GameEventType.SunflowerGrow, PayPrice);
    }

    protected override void Constructor() {
        EventDispatcher.Instance.Subscribe(GameEventType.SunflowerGrow, true, PayPrice);
        GrowRange = 1;
        WaterPrice = 2;
        SunPrice = 0;
        Price.Water = WaterPrice;
        Price.SunLight = SunPrice;
        SproutType = SproutType.Sunflower;
    }
    
}
