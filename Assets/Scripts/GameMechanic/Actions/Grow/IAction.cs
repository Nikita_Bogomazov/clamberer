﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class IAction : MonoBehaviour {
    private int _growRange;
    private int _waterPrice;
    private int _sunPrice;
    public int GrowRange {
        get { return _growRange; }
        set { _growRange = value; }
    }
    public int WaterPrice {
        get { return _waterPrice; }
        set { _waterPrice = value; }
    }
    public int SunPrice {
        get { return _sunPrice; }
        set { _sunPrice = value; }
    }
    public GameObject ПрефабПобега;

    protected CurrentLevelSettings CurrentLevelSettings;
    protected Resources Price;
    protected Building Building;
    protected Vine Vine;
    protected PathFinder PathFinder;
    protected SproutType SproutType;
    protected Sky Sky;
    protected WeatherController WeatherController;

    private void Start() {
        Vine = FindObjectOfType<Vine>();
        Building = FindObjectOfType<Building>();
        CurrentLevelSettings = FindObjectOfType<CurrentLevelSettings>();
        Sky = FindObjectOfType<Sky>();
        WeatherController = FindObjectOfType<WeatherController>();
        PathFinder = new PathFinder();
        Constructor();
    }

    void OnDestroy() {
        Destroyer();
    }

    public abstract void Action();

    protected abstract bool Limitation(XY to);

    protected abstract void Constructor();

    protected virtual void Destroyer() {

    }

    protected abstract void AddEvent();

    protected abstract void PayPrice(GameEventType type, Dictionary<Param, object> obj);

    protected virtual void Effect(GameEventType type, Dictionary<Param, object> obj) {
        
    }

    protected virtual void Remove(GameEventType type, Dictionary<Param, object> obj) {

    }
}
