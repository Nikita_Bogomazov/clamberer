﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SproutGrow : IAction {
    public override void Action() {
        if (!Limitation(Building.EntangleParams.Block))
            return;
        //
        //находим ближайший к конечной точке хедер лозы
        Vine.FindNearestSproutHead(Building.EntangleParams.BracingInBlock);
        //строим карту движения от найденой точки до конечной
        var moveMap = PathFinder.Astar(Vine.SproutHeadBracing, Building.EntangleParams.BracingInBlock, Building.Bracings, Building.Height * 3, Building.Width * 3);
        //говорим блоку что он будет оплетен
        Building.BuildingBlocks[Building.EntangleParams.Block.X, Building.EntangleParams.Block.Y].GetComponent<Block>().ChangeAction(OnClickBlockAction.ActivateInnerObject);
        Vine.SproutHeadBlock = Building.EntangleParams.Block;
        Vine.CreateSprout(moveMap, PathFinder._waveCount, ПрефабПобега,SproutType);
    }

    protected override void AddEvent() {
        EventDispatcher.Instance.DispatchEvent(GameEventType.SproutGrow);
    }

    protected override void PayPrice(GameEventType type, Dictionary<Param, object> obj) {
        CurrentLevelSettings.CurrentResources.SunLight -= SunPrice;
        CurrentLevelSettings.CurrentResources.Water -= WaterPrice;
    }

    protected override void Constructor() {
        EventDispatcher.Instance.Subscribe(GameEventType.SproutGrow, true, PayPrice);
        GrowRange = 1;
        WaterPrice = 1;
        SunPrice = 1;
        Price.Water = WaterPrice;
        Price.SunLight = SunPrice;
        SproutType = SproutType.SimpleSprout;
    }

    protected override void Destroyer() {
        EventDispatcher.Instance.UnSubscribe(GameEventType.SproutGrow, PayPrice);
    }

    protected override bool Limitation(XY to) {
        var lim = false;
        for (int i = 0; i < Building.BuildingBlocks.GetLength(0); i++) {
            for (int j = 0; j < Building.BuildingBlocks.GetLength(1); j++) {
                if (Building.BuildingBlocks[i, j].GetComponent<Block>().Entangled) {
                    if (Mathf.Abs(Building.BuildingBlocks[i, j].GetComponent<Block>().Number.X - to.X) <= GrowRange
                        && Mathf.Abs(Building.BuildingBlocks[i, j].GetComponent<Block>().Number.Y - to.Y) <= GrowRange) {
                        lim = true;
                    }
                }
            }
        }

        if (CurrentLevelSettings.CurrentResources.Water < Price.Water 
        || CurrentLevelSettings.CurrentResources.SunLight < Price.SunLight) {
            Debug.Log("Not enough resources");
            lim = false;
        }
        if (lim) {
            AddEvent();
            return true;
        }
        return false;
    }
}
