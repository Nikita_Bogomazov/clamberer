﻿using UnityEngine;
using System.Collections;

public struct Resources {
    public int SunLight;
    public int Water;
    public int Food;
    public int Money;
}
public class CurrentLevelSettings : MonoBehaviour{
    public Resources CurrentResources;
    public int Actions;

    private void Awake() {
        CurrentResources.SunLight = 15;
        CurrentResources.Water = 15;
        CurrentResources.Food = 2;
        CurrentResources.Money = 0;
        Actions = 3;
    }
}
