﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurnController : MonoBehaviour {
    private const int ACTIONS_ON_LEVEL = 3;

    private CurrentLevelSettings _currentLevelSettings;

    void Start() {
        _currentLevelSettings = GetComponent<CurrentLevelSettings>();
        EventDispatcher.Instance.Subscribe(GameEventType.CommitActs, false, ActionCountCheker);
        EventDispatcher.Instance.Subscribe(GameEventType.TurnEnd, false, RefillActions);
    }

    public void ActionCountCheker(GameEventType type, Dictionary<Param, object> obj) {
        _currentLevelSettings.Actions--;
        if (_currentLevelSettings.Actions <= 0) {
            EventDispatcher.Instance.DispatchEvent(GameEventType.TurnEnd);
        }
    }

    public void TurnEnd(GameEventType type, Dictionary<Param, object> obj) {
        
    }

    private void RefillActions(GameEventType type, Dictionary<Param, object> obj) { _currentLevelSettings.Actions = ACTIONS_ON_LEVEL; }

}
