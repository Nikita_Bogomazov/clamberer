﻿using UnityEngine;
using System.Collections;

public struct XY {
    public XY(int x, int y) {
        X = x;
        Y = y;
    }
    public int X;
    public int Y;
}

public struct EntangleParams {
    public XY Block;
    public XY BracingInBlock;
}

public class Building : MonoBehaviour {
    private const int BRACINGS_IN_ROW = 3;
    private const int BRACINGS_IN_COLUMN = 3;
    private const int SPROUT_RANGE = 1;

    public Canvas BuildingCanvas;
    public GameObject ПрефабБлока;
    public int Height;
    public int Width;
    public float XShift;
    public float YShift;
    public EntangleParams EntangleParams;
    public GameObject[,] BuildingBlocks;
    public GameObject[,] Bracings;

    private Vine _vine;
    private Sky _sky;
    private CurrentLevelSettings _currentLevelSettings;

    void Start() {
        Height = 10;
        Width = 3;
        XShift = 4.2F;
        YShift = 4.2F;
        _vine = FindObjectOfType<Vine>();
        _currentLevelSettings = FindObjectOfType<CurrentLevelSettings>();
        _sky = FindObjectOfType<Sky>();
        Build();
        _sky.StartBuild();
    }

    public void Build() {
        //mb should be static
        BuildingBlocks = new GameObject[Height,Width];
        Bracings = new GameObject[Height * BRACINGS_IN_COLUMN, Width * BRACINGS_IN_ROW];
        float _variableXShift = XShift, _variableYShift = YShift;
        var avalue = 0;
        for (int i = 0; i < Height; i++) {
            for (int j = 0; j < Width; j++) {
                var instance = Instantiate(ПрефабБлока);
                instance.transform.SetParent(BuildingCanvas.transform,false);
                instance.GetComponent<Block>().Number = new XY(i,j);
                instance.GetComponent<Block>().ChangeAction(OnClickBlockAction.Entangle);
                if (i > 2) { instance.GetComponent<Block>().ChangeStaticState(Random.Range(0, 10)); } else {
                    instance.GetComponent<Block>().ChangeStaticState(0);
                }
                BuildingBlocks[i, j] = instance;
                BuildingBlocks[i, j].transform.position = new Vector3(transform.position.x + _variableXShift,transform.position.y + _variableYShift);
                _variableXShift += XShift;
            }
                var value = 0;
                for (int m = avalue; m < (avalue + BRACINGS_IN_COLUMN); m++) {
                    var help = 0;
                    for (int j = 0; j < Width; j++) {
                    for (int k = value; k < (value + BRACINGS_IN_ROW); k++) {
                        var bracing = BuildingBlocks[i, j].GetComponent<Block>().Bracings[k];
                        bracing.GetComponent<Bracing>().Number = new XY(m,help);
                        Bracings[m, help++] = bracing;
                    }
                }
                value += BRACINGS_IN_ROW;
            }
            avalue += BRACINGS_IN_COLUMN;
            _variableXShift = XShift;
            _variableYShift += YShift;
        }
        BuildingBlocks[0, 1].GetComponent<Block>().ChangeAction(OnClickBlockAction.ActivateInnerObject);
    }

    public int FindSproutsOnTheLine(int lineIndex, int begin) {
        var value = 0;
        if (begin == 0) begin = Height;
        for (int i = 0; i < begin; i++) {
            if (BuildingBlocks[i, lineIndex].GetComponent<Block>().Entangled) {
                value++;
            }
        }
        return value;
    }
}
