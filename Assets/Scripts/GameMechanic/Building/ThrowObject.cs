﻿using UnityEngine;
using System.Collections;

public class ThrowObject : MonoBehaviour {
    public XY Number;

    private Building _building;
    private Vine _vine;

    private void Start() { _building = FindObjectOfType<Building>(); }

    void OnTriggerEnter2D(Collider2D other) {
        if (!other.CompareTag("Sprout"))
            return;
        Fall(Number);
    }

    public void Fall(XY startPosition) {
        _building = FindObjectOfType<Building>();
        _vine = FindObjectOfType<Vine>();
        gameObject.SetActive(false);
        for (int i = startPosition.X; i >= 0; i--) {
            if (_building.BuildingBlocks[i, startPosition.Y].GetComponent<Block>().Entangled) {
                _building.BuildingBlocks[i, startPosition.Y].GetComponent<Block>().ChangeAction(OnClickBlockAction.Entangle);
                foreach (var bracing in _building.BuildingBlocks[i, startPosition.Y].GetComponent<Block>().Bracings) {
                    //находим себя в графе
                    var value = _vine.SproutGraph.ContainsNode(bracing.GetComponent<Bracing>().EngangleSprout);
                    //выпиливаемся
                    if (value != null) {
                        _vine.RemoveSprout(value);
                    }
                }
                return;
            }
        }
    }
}
