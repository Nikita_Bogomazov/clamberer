﻿using UnityEngine;
using System.Collections;

public enum SproutType {
    SimpleSprout,Sunflower,
}

public class Sprout : ISprout {
    public XY EntangledBracing;
    public XY EntangledBlock;
    public SproutType Type = SproutType.SimpleSprout;

    private Vine _vine;
	void Start () { _vine = FindObjectOfType<Vine>(); }

    void OnTriggerEnter2D(Collider2D other) {
        if (!other.CompareTag("DangerObject"))
            return;
        //other.gameObject.SetActive(false);
        ////находим себя в графе
        //var value = _vine.SproutGraph.ContainsNode(gameObject);
        ////выпиливаемся
        //_vine.RemoveSprout(value);
        //Destroy(other.gameObject);
    }
}
