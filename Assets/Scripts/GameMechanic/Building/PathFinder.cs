﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFinder {
    private int[,] _clearMap;
    private XY[] _move = new XY[8*8];
    private bool _wayIsFind = true;
    public int _waveCount = 0;
    private int _mapHeight;
    private int _mapWitdh;

    public XY[] Astar(XY start, XY target, GameObject[,] bracings, int height, int width) {
        _mapHeight = height;
        _mapWitdh = width;
        var add = true;
        _wayIsFind = true;
        _clearMap = new int[_mapHeight, _mapWitdh];
        int x, y, step = 0;
        for (y = 0; y < _mapHeight; y++)
            for (x = 0; x < _mapWitdh; x++) {
                if (bracings[y,x].GetComponent<Bracing>().Empty == false)
                    _clearMap[y, x] = -2; //индикатор стены
                else
                    _clearMap[y, x] = -1; //индикатор еще не ступали сюда
            }
        _clearMap[target.X, target.Y] = 0; //Начинаем с финиша
        while (add) {
            add = false;
            for (y = 0; y < _mapWitdh; y++)
                for (x = 0; x < _mapHeight; x++) {
                    if (_clearMap[x, y] == step) {
                        //Ставим значение шага+1 в соседние ячейки (если они проходимы)
                        if (x - 1 >= 0 && _clearMap[x - 1, y] != -2 && _clearMap[x - 1, y] == -1)
                            _clearMap[x - 1, y] = step + 1;
                        if (y - 1 >= 0 && _clearMap[x, y - 1] != -2 && _clearMap[x, y - 1] == -1)
                            _clearMap[x, y - 1] = step + 1;
                        if (x + 1 < _mapHeight && _clearMap[x + 1, y] != -2 && _clearMap[x + 1, y] == -1)
                            _clearMap[x + 1, y] = step + 1;
                        if (y + 1 < _mapWitdh && _clearMap[x, y + 1] != -2 && _clearMap[x, y + 1] == -1)
                            _clearMap[x, y + 1] = step + 1;
                    }
                }
            step++;
            add = true;
            if (_clearMap[start.X, start.Y] != -1) {
                add = false;
            }
            if (step > _mapWitdh*_mapHeight) {
                //решение не найдено
                Debug.Log("way not found");
                return null;
            }
        }

        if (_wayIsFind) {
            XY value = new XY(start.X,start.Y);
            BuildMap(value, step);
            return _move;
        }
        Debug.Log("way not found");
        return null;
    }

    private void BuildMap(XY value, int step) {
        while (step-- > 0) {
            int randomCount = 0;
            List<int> pass = new List<int>();
            System.Random rnd = new System.Random();
            while (randomCount < 4) {
                var rand = rnd.Next(1, 5);
                bool brk = false;
                foreach (var p in pass) {
                    if (rand == p) {
                        brk = true;
                        break;
                    }
                }
                if (brk)
                    continue;
                switch (rand) {
                    case 1:
                        if (value.X - 1 >= 0) {
                            if (_clearMap[value.X - 1, value.Y] == step)
                                value.X = value.X - 1;
                        }
                        pass.Add(rand);
                        break;
                    case 2:
                        pass.Add(rand);
                        if (value.X + 1 < _mapHeight) {
                            if (_clearMap[value.X + 1, value.Y] == step)
                                value.X = value.X + 1;
                        }
                        break;
                    case 3:
                        pass.Add(rand);
                        if (value.Y - 1 >= 0) {
                            if (_clearMap[value.X, value.Y - 1] == step)
                                value.Y = value.Y - 1;
                        }
                        break;
                    case 4:
                        pass.Add(rand);
                        if (value.Y + 1 < _mapWitdh) {
                            if (_clearMap[value.X, value.Y + 1] == step)
                                value.Y = value.Y + 1;
                        }
                        break;
                }
                randomCount++;
            }
            _move[_waveCount] = value;
            _waveCount++;
        }
    }
}
