﻿using UnityEngine;
using System.Collections;

public enum BlockState {
    Empty,
    OpenWindow,
    CloseWindow,
}

public abstract class IBlock : MonoBehaviour {
    
}
