﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine.UI;

public enum OnClickBlockAction {
    Entangle, ActivateInnerObject,
}

public enum StaticInnerActions {
    ThrowSomething,
}

public class Block : IBlock {
    public bool Entangled;
    public XY Number;
    public GameObject[] Bracings = new GameObject[9];
    public BlockState State = BlockState.Empty;
    public GameObject InnerStaticObject;
    public GameObject InnerActiveObject;
    public GameObject ПрефабВещи;

    private Button _button;
    private Building _building;
    private bool _inVision;
    private MenuParts _menuParts;
    private bool _innerObjectActivate;

	void Start () {
	    _building = FindObjectOfType<Building>();
	    _button = GetComponent<Button>();
	    _menuParts = FindObjectOfType<MenuParts>();
        EventDispatcher.Instance.Subscribe(GameEventType.TurnEnd, false,ActivateStaticInnerObject);
        EventDispatcher.Instance.Subscribe(GameEventType.TurnEnd, false, UpdateActiveObject);
        EventDispatcher.Instance.Subscribe(GameEventType.DisableBlocks, false, DisableSelf);
        EventDispatcher.Instance.Subscribe(GameEventType.EnableBlocks, false, EnableSelf);
	}

    private void OnBecameVisible() {
        _inVision = true;
    }

    private void OnBecameInvisible() {
        _inVision = false;
    }

    public void ChangeAction(OnClickBlockAction action) {
        _button = GetComponent<Button>();
        _button.onClick.RemoveAllListeners();
        switch (action) {
                case OnClickBlockAction.Entangle:
                _button.onClick.AddListener(Entangle);
                Entangled = false;
                break;
                case OnClickBlockAction.ActivateInnerObject:
                _button.onClick.AddListener(ActivateInnerObject);
                Entangled = true;
                break;
        }
    }

    public void ActivateStaticInnerObject(GameEventType type, Dictionary<Param, object> par2) {
        //каждый ход
        if (State == BlockState.OpenWindow && _inVision && Random.Range(0,8) == 0 && !Entangled) {
            var obj = Instantiate(ПрефабВещи);
            var rig = obj.GetComponent<Rigidbody2D>();
            rig.isKinematic = true;
            obj.transform.position = new Vector2(transform.position.x, transform.position.y);
            rig.isKinematic = false;
            obj.GetComponent<ThrowObject>().Number = Number;
        }
    }

    public void ActivateInnerObject() {
        //при нажатии
        if(_innerObjectActivate)return;
        int action = Random.Range(0, 3);
        var dict = new Dictionary<Param, object>();
        if (State == BlockState.OpenWindow) {
            switch (action) {
                    case 1:
                    //money
                    dict.Add(Param.ResourcesCount, 1);
                    EventDispatcher.Instance.DispatchEvent(GameEventType.AddMoney,dict);
                    break;
                    case 2:
                    //food
                    dict.Add(Param.ResourcesCount, 1);
                    EventDispatcher.Instance.DispatchEvent(GameEventType.AddFood,dict);
                    break;
                    case 3:
                    //damage
                    break;
                    default:
                    break;
            }
        }
        _innerObjectActivate = true;;
        EventDispatcher.Instance.DispatchEvent(GameEventType.CommitActs);
    }

    public void Entangle() {
        var random = Random.Range(0,8);
        while (random == 0 || random == 2 ||
        random == 6 || random == 8) {
            random = Random.Range(0, 8);
        }
        //action
        _building.EntangleParams.Block = Number;
        _building.EntangleParams.BracingInBlock = Bracings[random].GetComponent<Bracing>().Number;
        _menuParts.OpenBuildMenu(); 
    }

    public void PlayActiveComponent() {
        
    }

    public void ChangeStaticState(int state) {
        switch (state) {
            case 1:
                State = BlockState.OpenWindow;
                break;
            case 2:
                State = BlockState.CloseWindow;
                break;
            default:
                State = BlockState.Empty;
                break;
        }
        InnerStaticObject.GetComponent<Animator>().SetInteger("State", (int)State);
    }

    private void DisableSelf(GameEventType type, Dictionary<Param, object> obj) {
        GetComponent<Button>().interactable = false;
    }

    private void EnableSelf(GameEventType type, Dictionary<Param, object> obj) {
        GetComponent<Button>().interactable = true;
    }

    private void UpdateActiveObject(GameEventType type, Dictionary<Param, object> obj) { _innerObjectActivate = false; }
}
