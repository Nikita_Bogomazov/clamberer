﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vine : MonoBehaviour {
    //public GameObject ПрефабПобега;
    public XY SproutHeadBracing;
    public XY SproutHeadBlock;
    public Graph<GameObject> SproutGraph = new Graph<GameObject>();

    private List<GameObject> _onRemove = new List<GameObject>();
    private Building _building;
    private GameObject _head;
    private float _rotate;

    void Start() {
        _building = FindObjectOfType<Building>();
        SproutHeadBracing = new XY(0,4);
        SproutHeadBlock = new XY(0,1);
    }

    public void CreateSprout(XY[] moveMap, int waveCount, GameObject ПрефабПобега, SproutType rootType) {
        XY one;
        XY two;
        for (int i = -1; i < waveCount; i++) {
            var instance = Instantiate(ПрефабПобега);
            one = (i == -1) ? SproutHeadBracing : moveMap[i];
            two = moveMap[i + 1];
            RotateSprout(_building.Bracings, one, two);
            instance.transform.localEulerAngles = new Vector3(0, 0, _rotate);
            //переделать под функцию от буилдинг
            instance.transform.position = new Vector3(_building.Bracings[one.X, one.Y].transform.position.x,
                _building.Bracings[one.X, one.Y].transform.position.y);
            //устанавливаем тип корня растения
            if (i == 1) instance.GetComponent<Sprout>().Type = rootType;
            if (i != -1) {
                _building.Bracings[one.X, one.Y].GetComponent<Bracing>().EngangleSprout = instance;
                //Debug.Log("Rewrite" + " " + i);
            }
            instance.GetComponent<Sprout>().EntangledBracing = one; //?
            instance.GetComponent<Sprout>().EntangledBlock = SproutHeadBlock;
            SproutGraph.AddNode(instance);
            if (SproutGraph.Count > 1) {
                //ищем добавленную ноду в графе
                var value = SproutGraph.ContainsNode(instance);
                //строим связь "хедер" -> "текущая нода"
                var head = SproutGraph.ContainsNode(_head);
                SproutGraph.AddDirectedEdge(head, value);
            }
            //обновляем хедер
            _head = instance;
        }
        SproutHeadBracing = moveMap[waveCount - 1];
        EventDispatcher.Instance.DispatchEvent(GameEventType.CommitActs);
    }

    public void FindNearestSproutHead(XY to) {
        if (SproutGraph.Count < 1)
            return;
        var first = false;
        GameObject min = null;
        float minDistance = 0;
        float distance = 0;
        foreach (var va in SproutGraph.Nodes) {
            if (!first) {
                if (_head == null) {
                    distance = Vector2.Distance(new Vector2(to.X, to.Y),
                        new Vector2(va.Value.GetComponent<Sprout>().EntangledBracing.X,
                            va.Value.GetComponent<Sprout>().EntangledBracing.Y));
                    min = va.Value;
                } else {
                    distance = Vector2.Distance(new Vector2(to.X, to.Y),
                        new Vector2(_head.GetComponent<Sprout>().EntangledBracing.X,
                            _head.GetComponent<Sprout>().EntangledBracing.Y));
                    min = _head;
                }
                minDistance = distance;
                first = true;
            } else {
                distance = Vector2.Distance(new Vector2(to.X, to.Y),
                    new Vector2(va.Value.GetComponent<Sprout>().EntangledBracing.X,
                        va.Value.GetComponent<Sprout>().EntangledBracing.Y));
                if (distance < minDistance) {
                    minDistance = distance;
                    min = va.Value;
                }
            }
        }
        _head = min;
        SproutHeadBracing = _head.GetComponent<Sprout>().EntangledBracing;
    }

    //рекурсивно проходим граф в глубину начиная с текущей ноды
    public void RemoveSprout(GraphNode<GameObject> node) {
        _onRemove = new List<GameObject>();
        _onRemove.Add(node.Value);
        RecursiveRemove(node.Neighbors);
        foreach (var remove in _onRemove) {
            SproutGraph.Remove(remove);
            _building.Bracings[remove.GetComponent<Sprout>().EntangledBracing.X, remove.GetComponent<Sprout>().EntangledBracing.Y]
                .GetComponent<Bracing>().EngangleSprout = null;
            if (IsBlockFullEmpty(remove.GetComponent<Sprout>().EntangledBlock)) {
                _building.BuildingBlocks[remove.GetComponent<Sprout>().EntangledBlock.X, remove.GetComponent<Sprout>().EntangledBlock.Y]
                    .GetComponent<Block>().ChangeAction(OnClickBlockAction.Entangle);
            }
            Destroy(remove);
        }
    }

    private bool IsBlockFullEmpty(XY value) {
        foreach (var bracing in _building.BuildingBlocks[value.X, value.Y].GetComponent<Block>().Bracings) {
            if (bracing.GetComponent<Bracing>().EngangleSprout != null) { return false; }
        }
        return true;
    }

    private void RecursiveRemove(NodeList<GameObject> node) {
        foreach (var n in node) {
            RemoveEvent(n.Value.GetComponent<Sprout>().Type);
            _onRemove.Add(n.Value);
            if (n.Neighbors != null)
                RecursiveRemove(n.Neighbors);
        }
    }

    private void RemoveEvent(SproutType type) {
        switch (type) {
                case SproutType.Sunflower:
                EventDispatcher.Instance.DispatchEvent(GameEventType.SunflowerRemove);
                break;
        }
    }

    private void RotateSprout(GameObject[,] bracings, XY one, XY two) {
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    if (bracings[one.X, one.Y].transform.position.x >
                        bracings[two.X, two.Y].transform.position.x) {
                        //left
                        _rotate = 0;
                    }
                    break;
                case 1:
                    if (bracings[one.X, one.Y].transform.position.x <
                        bracings[two.X, two.Y].transform.position.x) {
                        //right
                        _rotate = 180;
                    }
                    break;
                case 2:
                    if (bracings[one.X, one.Y].transform.position.y <
                        bracings[two.X, two.Y].transform.position.y) {
                        //top
                        _rotate = 270;
                    }
                    break;
                case 3:
                    if (bracings[one.X, one.Y].transform.position.y >
                        bracings[two.X, two.Y].transform.position.y) {
                        //bot
                        _rotate = 90;
                    }
                    break;
            }
        }
    }
}
