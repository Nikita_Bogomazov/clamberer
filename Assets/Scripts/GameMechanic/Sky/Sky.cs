﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Sky : MonoBehaviour {
    public const int Отступ = 3;

    public GameObject ПрефабНеба;
    public Canvas BuildingCanvas;
    public GameObject[,] SkyParts;
    public int Height;
    public int Width;
    public XY CloudParam;

    private Building _building;
    private int _additionalWidth;
    private float _XShift;
    private float _YShift;

    void Start() {
        
    }

    public void StartBuild() {
        _building = FindObjectOfType<Building>();
        Height = _building.Height;
        Width = _building.Width + (Отступ*2);
        _XShift = _building.XShift;
        _YShift = _building.YShift;
        Build();
    }

    private void Build() {
        SkyParts = new GameObject[Height, Width];
        float _variableXShift = _XShift, _variableYShift = _YShift;
        var avalue = 0;
        for (int i = 0; i < Height; i++) {
            for (int j = 0; j < Width; j++) {
                var instance = Instantiate(ПрефабНеба);
                instance.transform.SetParent(BuildingCanvas.transform, false);
                instance.GetComponent<SkyParts>().Number = new XY(i,j);
                if (j > (Отступ - 1) && j < (Отступ + _building.Width)) instance.SetActive(false);
                SkyParts[i, j] = instance;
                SkyParts[i, j].transform.position = new Vector3(transform.position.x + _variableXShift, transform.position.y + _variableYShift);
                _variableXShift += _XShift;
            }
            _variableXShift = _XShift;
            _variableYShift += _YShift;
        }
    }
}
