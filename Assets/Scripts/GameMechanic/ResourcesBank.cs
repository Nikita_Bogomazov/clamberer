﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourcesBank : MonoBehaviour {
    private CurrentLevelSettings _currentLevelSettings;

    void Awake() {
        _currentLevelSettings = FindObjectOfType<CurrentLevelSettings>();
        EventDispatcher.Instance.Subscribe(GameEventType.AddFood, false, AddFood);
        EventDispatcher.Instance.Subscribe(GameEventType.AddMoney, false, AddMoney);
        EventDispatcher.Instance.Subscribe(GameEventType.WaterDrop, false, Rain);
        EventDispatcher.Instance.Subscribe(GameEventType.SunShine, false, SunShine);
    }

    public void Rain(GameEventType type, Dictionary<Param, object> obj) {
        int value = (int) obj[Param.ResourcesCount];
        Debug.Log("+" + value + " Water");
        _currentLevelSettings.CurrentResources.Water += value;
    }

    public void SunShine(GameEventType type, Dictionary<Param, object> obj) {
        int value = (int)obj[Param.ResourcesCount];
        Debug.Log("+" + value + " Sun");
        _currentLevelSettings.CurrentResources.SunLight += value;
    }

    public void AddMoney(GameEventType type, Dictionary<Param, object> obj) {
        int value = (int)obj[Param.ResourcesCount];
        Debug.Log("+" + value + " Money");
        _currentLevelSettings.CurrentResources.Money += value;
    }

    public void AddFood(GameEventType type, Dictionary<Param, object> obj) {
        int value = (int)obj[Param.ResourcesCount];
        Debug.Log("+" + value + " Food");
        _currentLevelSettings.CurrentResources.Food += value;
    }
}
