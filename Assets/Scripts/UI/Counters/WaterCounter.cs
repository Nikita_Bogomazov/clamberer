﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WaterCounter : ICounter {
    protected override void ChangeValue(GameEventType type, Dictionary<Param, object> obj) {
        _text.text = _cureCurrentLevelSettings.CurrentResources.Water.ToString();
    }

    protected override void ChangeValue() {
        _text.text = _cureCurrentLevelSettings.CurrentResources.Water.ToString();
    }
}
