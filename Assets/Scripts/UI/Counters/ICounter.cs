﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public abstract class ICounter : MonoBehaviour {
    protected Text _text;
    protected CurrentLevelSettings _cureCurrentLevelSettings;

    void Start() {
        _text = GetComponent<Text>();
        _cureCurrentLevelSettings = FindObjectOfType<CurrentLevelSettings>();
        EventDispatcher.Instance.Subscribe(GameEventType.CommitActs, false, ChangeValue);
        ChangeValue();
    }
    protected abstract void ChangeValue();
    protected abstract void ChangeValue(GameEventType type, Dictionary<Param, object> obj);
}
