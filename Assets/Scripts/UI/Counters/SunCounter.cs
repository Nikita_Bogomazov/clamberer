﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SunCounter : ICounter {
    protected override void ChangeValue(GameEventType type, Dictionary<Param, object> obj) {
        _text.text = _cureCurrentLevelSettings.CurrentResources.SunLight.ToString();
    }

    protected override void ChangeValue() {
        _text.text = _cureCurrentLevelSettings.CurrentResources.SunLight.ToString();
    }
}
