﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoneyCounter : ICounter {
    protected override void ChangeValue(GameEventType type, Dictionary<Param, object> obj) {
        _text.text = _cureCurrentLevelSettings.CurrentResources.Money.ToString();
    }

    protected override void ChangeValue() {
        _text.text = _cureCurrentLevelSettings.CurrentResources.Money.ToString();
    }
}
