﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum OnClickSkyAction {
    CreateCloud, Nothing,
}

public class SkyParts : MonoBehaviour {
    public XY Number;
    public bool Engaged;

    private MenuParts _menuParts;
    private Sky _sky;
    private Button _button;

    private void Start() {
        _menuParts = FindObjectOfType<MenuParts>();
        _sky = FindObjectOfType<Sky>();
        EventDispatcher.Instance.Subscribe(GameEventType.DisableBlocks, false, DisableSelf);
        EventDispatcher.Instance.Subscribe(GameEventType.EnableBlocks, false, EnableSelf);
        ChangeAction(OnClickSkyAction.CreateCloud);
    }

    public void ChangeAction(OnClickSkyAction action) {
        _button = GetComponent<Button>();
        _button.onClick.RemoveAllListeners();
        switch (action) {
                case OnClickSkyAction.CreateCloud:
                _button.onClick.AddListener(CreateCloud);
                Engaged = false;
                break;
                case OnClickSkyAction.Nothing:
                Engaged = true;
                break;
        }
    }

    private void CreateCloud() {
        _sky.CloudParam = Number;
        _menuParts.OpenSkyMenu();  
    }

    private void DisableSelf(GameEventType type, Dictionary<Param, object> obj) {
        GetComponent<Button>().interactable = false;
    }

    private void EnableSelf(GameEventType type, Dictionary<Param, object> obj) {
        GetComponent<Button>().interactable = true;
    }
}
