﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MenuParts : MonoBehaviour {
    public GameObject Handler;
    public GameObject BuildWindow;
    public GameObject SkyWindow;
    public GameObject[] BuildActions = new GameObject[10];
    public GameObject[] BuildEmpty = new GameObject[3];
    public GameObject[] SkyActions = new GameObject[10];
    public GameObject[] SkyEmpty = new GameObject[3];

    private List<GameObject> _buildActionsList;
    private List<GameObject> _skyActionsList;

	void Start () {
	    
	}
    public void OpenSkyMenu() {
        Handler.SetActive(true);
        SkyWindow.SetActive(true);
        _skyActionsList = new List<GameObject>();
        for (int i = 0; i < SkyEmpty.GetLength(0); i++) {
            if (SkyActions[i] == null)
                break;
            var instance = Instantiate(SkyActions[i]);
            instance.transform.SetParent(SkyEmpty[i].transform);
            instance.transform.localScale = new Vector3(1, 1, 1);
            var rect = instance.GetComponent<RectTransform>();
            rect.localPosition = new Vector3(0, 0, 0);
            instance.GetComponent<Button>().onClick.AddListener(CloseSkyMenu);
            _skyActionsList.Add(instance);
        }
        EventDispatcher.Instance.DispatchEvent(GameEventType.DisableBlocks);
    }

    public void CloseSkyMenu() {
        foreach (var action in _skyActionsList) {
            Destroy(action);
        }
        SkyWindow.SetActive(false);
        Handler.SetActive(false);
        EventDispatcher.Instance.DispatchEvent(GameEventType.EnableBlocks);
    }

    public void OpenBuildMenu() {
        Handler.SetActive(true);
        BuildWindow.SetActive(true);
        _buildActionsList = new List<GameObject>();
        for (int i = 0; i < BuildEmpty.GetLength(0); i++) {
            if(BuildActions[i] == null)break;
            var instance = Instantiate(BuildActions[i]);
            instance.transform.SetParent(BuildEmpty[i].transform);
            instance.transform.localScale = new Vector3(1,1,1);
            var rect = instance.GetComponent<RectTransform>();
            rect.localPosition = new Vector3(0,0,0);
            instance.GetComponent<Button>().onClick.AddListener(CloseBuildMenu);
            _buildActionsList.Add(instance);
        }
        EventDispatcher.Instance.DispatchEvent(GameEventType.DisableBlocks);
    }

    public void CloseBuildMenu() {
        foreach (var action in _buildActionsList) {
            Destroy(action);
        }
        BuildWindow.SetActive(false);
        Handler.SetActive(false);
        EventDispatcher.Instance.DispatchEvent(GameEventType.EnableBlocks);
    }
}
