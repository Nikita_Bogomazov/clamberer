﻿using UnityEngine;
using System.Collections;

public class Converter : MonoBehaviour {
    public int XValue;

    private CurrentLevelSettings _currentLevelSettings;

    void Start() { _currentLevelSettings = FindObjectOfType<CurrentLevelSettings>(); }

    public void ConvertWater() {
        if (_currentLevelSettings.CurrentResources.Water < XValue)
            return;
        _currentLevelSettings.CurrentResources.Water -= 2;
        _currentLevelSettings.CurrentResources.SunLight += 1;
        EventDispatcher.Instance.DispatchEvent(GameEventType.CommitActs);
    }

    public void ConvertSun() {
        if (_currentLevelSettings.CurrentResources.SunLight < XValue)
            return;
        _currentLevelSettings.CurrentResources.SunLight -= 2;
        _currentLevelSettings.CurrentResources.Water += 1;
        EventDispatcher.Instance.DispatchEvent(GameEventType.CommitActs);
    }
}
